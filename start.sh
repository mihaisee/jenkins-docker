#!/usr/bin/env bash

docker-compose build

docker run -t -d -e DOCKER_SOCKET="/var/run/docker.sock" -e DOCKER_BIN="/usr/bin/docker" -e JENKINS_GROUP="jenkins" -e JENKINS_USER="jenkins" -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):/usr/bin/docker -p 8080:8080 jenkinsdocker_jenkins