#!/bin/bash

chown ${JENKINS_USER}:${JENKINS_GROUP} ${DOCKER_SOCKET}
chown -R ${JENKINS_USER}:${JENKINS_GROUP} ${DOCKER_BIN}

mkdir -p /var/www/.composer/cache/repo/https---packagist.org/
mkdir -p /var/www/.composer/cache/files/

chmod 777 -R /var/www/.composer/

exec sudo -E -H -u jenkins bash -c /usr/local/bin/jenkins.sh


